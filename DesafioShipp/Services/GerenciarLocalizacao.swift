//
//  GerenciarLocalizacao.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 24/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import MapKit
import CoreLocation
import UIKit

class GerenciarLocalizacao {
    func tratarAcessoLocal() -> UIAlertController {
            let alertaController = UIAlertController(title: "Permissão de localização",
                                                     message: "Necessário permissão para acesso à sua localização para realizar os pedidos",
                                                     preferredStyle: .alert )
            
            let acaoConfiguracoes = UIAlertAction(title: "Abrir configurações", style: .default , handler: { (alertaConfiguracoes) in
            if let configuracoes = NSURL(string: UIApplication.openSettingsURLString ) {
                    UIApplication.shared.open( configuracoes as URL )
                }})
            
            let acaoCancelar = UIAlertAction(title: "Cancelar", style: .default , handler: nil )
            alertaController.addAction( acaoConfiguracoes )
            alertaController.addAction( acaoCancelar )
            return alertaController
        }}

extension CheckoutViewController {
    func consultarEndereco() {
        let VCLocation = gerenciadorLocalizacao.location!
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(VCLocation) { (placemarksArray, error) in
            //print(placemarksArray!)
                    let placemark = placemarksArray?[0]
                    let address = "\(placemark?.thoroughfare ?? ""), \(placemark?.subThoroughfare ?? ""), \(placemark?.locality ?? ""), \(placemark?.subLocality ?? ""), \(placemark?.administrativeArea ?? ""), \(placemark?.postalCode ?? ""), \(placemark?.country ?? "")"
            self.endUsuarioLabel.text = "\(address)"
        }}}
