//
//  FormularioPOST.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension PrecoViewController {
    func consultarPrecosAPI() {
        latUS = receberCoordinateUsuario?.latitude
        longUS = receberCoordinateUsuario?.longitude
        
        let latES = placeAPI?.coordinate.latitude
        let longES = placeAPI?.coordinate.longitude
        let valorDigitado = precoDigitado.text
        
        let parametrosJSON = [ "store_latitude": latES!,
        "store_longitude": longES!,
        "user_latitude": latUS!,
        "user_longitude": longUS!,
        "value": Double((valorDigitado?.removeFormatAmount())!)
            ]
        print(parametrosJSON)
        
        let jsonData = try? JSONSerialization.data(withJSONObject: parametrosJSON)
        
        let url = URL(string: "https://d9eqa4nu35.execute-api.sa-east-1.amazonaws.com/evaluate")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            
            if let responseJSON = responseJSON as? [String: Any] {
                
                if let filtroTaxa = responseJSON["fee_value"] {
                    let valorTaxaFormatada = filtroTaxa as? Double
                    self.valorTaxa = Double(String(format:"%.2f", valorTaxaFormatada!))
                    print("Taxa: \(self.valorTaxa!)")
                }
                if let filtroValorProduto = responseJSON["product_value"] {
                    let valorProdutoFormatado = filtroValorProduto as? Double
                    self.valorProduto = Double(String(format:"%.2f", valorProdutoFormatado!))
                    print("Valor Produto: \(self.valorProduto!)")
                }
                
                if let filtroValorTotal = responseJSON["total_value"] {
                   let valorTotalFormatado = filtroValorTotal as? Double
                   self.valorTotal = Double(String(format:"%.2f", valorTotalFormatado!))
                    print("Valor Total: \(self.valorTotal!)")
                }
                if let filtroDistancia = responseJSON["distance"] {
                    let distanciaFormatada = filtroDistancia as? Double
                    self.distancia = Double(String(format:"%.2f", distanciaFormatada!))
                    print("Distancia: \(self.distancia!)")
                }}}
        task.resume()}}

extension CheckoutViewController {
    func validarCompraAPI() {
        
        let latitudeEstabelecimento = Double((estabelecimento?.coordinate.latitude)!)
        let longitudeEstabelecimento = Double((estabelecimento?.coordinate.longitude)!)
        let latitudeUsuario = Double(gerenciadorLocalizacao.location!.coordinate.latitude)
        let longitudeUsuario = Double(gerenciadorLocalizacao.location!.coordinate.longitude)
        
        let parametrosJSON = [ "store_latitude": latitudeEstabelecimento,
        "store_longitude": longitudeEstabelecimento,
        "user_latitude": latitudeUsuario,
        "user_longitude": longitudeUsuario,
        "card_number": "\(numeroDoCartao!)",                //1111111111111111,
        "cvv": "\(cvvDoCartao!)",                           //789,
        "expiry_date": "\(validadeDoCartao!)",              //01/18,
        "value": receberValorEstimado! ] as [String : Any]
        
        print(parametrosJSON)
        
        let jsonData = try? JSONSerialization.data(withJSONObject: parametrosJSON)
        let url = URL(string: "https://mdk3ljy26k.execute-api.sa-east-1.amazonaws.com/order")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                
                if let filtroValor = responseJSON["value"] {
                    self.valorTotalFeedbackAPI = filtroValor as? Double
                    print("Valor: \(filtroValor)")
                }
                
                if let filtroMensagem = responseJSON["message"] {
                    self.mensagemFeedbackAPI = "\(filtroMensagem)"
                    print("Mensagem: \(filtroMensagem)"
                    
    )}}}
        task.resume()
    }
}
