//
//  TratarDadosViewControllers.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension CheckoutViewController {
    
    func mandarDadosParaLabels() {
        if receberNomeEstabelecimento != nil {
        nomeLabel.text = receberNomeEstabelecimento
        }else{ nomeLabel.text = "Erro" }
        
        if receberEndEstabelecimento != nil {
        endLabel.text = receberEndEstabelecimento
        }else{ endLabel.text = "Erro" }
        
        if receberProdutoDigitado != nil {
        produtoLabel.text = receberProdutoDigitado
        }else{ produtoLabel.text = "Erro" }
        
        if receberEndUsuario != nil {
        endUsuarioLabel.text = receberEndUsuario
        }else{ endUsuarioLabel.text = "Erro" }
        
        if receberDistancia != nil {
        formatarDistancia()
        }else{ distanciaLabel.text = "Erro" }
        
        formatarValores()}
    
    // Formatar Valores e jogar para Labels
    func formatarValores() {
        let formatterCurrency = NumberFormatter()
        formatterCurrency.locale = Locale(identifier: "pt_BR")
        formatterCurrency.numberStyle = .currency
        
        if receberValorEstimado != nil {
            let valorEstimadoFormat = formatterCurrency.string(from: receberValorEstimado! as NSNumber) ?? ""
            valorEstimadoLabel.text = "\(valorEstimadoFormat)"
        }else{ valorEstimadoLabel.text = "Erro ao formatar Valor"}
        if receberTaxa != nil {
            let valorTaxaFormat = formatterCurrency.string(from: receberTaxa! as NSNumber) ?? ""
            taxaEntregaLabel.text = "\(valorTaxaFormat)"
        }else{ valorEstimadoLabel.text = "Erro ao formatar Valor"}
        if receberValorTotal != nil {
            let valorTotalFormat = formatterCurrency.string(from: receberValorTotal! as NSNumber) ?? ""
            valorTotalLabel.text = "\(valorTotalFormat)"
        }else{ valorEstimadoLabel.text = "Erro ao formatar Valor"}}
    
    // Formatar Distancia e jogar para Label
    func formatarDistancia() {
        let formatterDistancia = NumberFormatter()
        formatterDistancia.locale = Locale(identifier: "pt_BR")
        formatterDistancia.numberStyle = .decimal
        if receberDistancia != nil {
            if let distanciaFormat = formatterDistancia.string(from: receberDistancia! as NSNumber) {
                distanciaLabel.text = "\(String(describing: distanciaFormat)) Km"
                }}else{ distanciaLabel.text = "Erro ao formatar distancia" }}}
