//
//  GerenciarDadosCartaoDeCredito.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension CadastrarCartaoViewController {
    
    func recuperarDadosCartao() {
    let recuprarNumeroCartao = UserDefaults.standard.object(forKey: "numeroDoCartao") as? String
    let recuprarNomeCartao = UserDefaults.standard.object(forKey: "nomeDoCartao")  as? String
    let recuprarValidadeCartao = UserDefaults.standard.object(forKey: "validadeDoCartao")  as? String
    let recuprarCVVCartao = UserDefaults.standard.object(forKey: "cvvDoCartao")  as? String
    
    if recuprarNumeroCartao != nil && recuprarNumeroCartao != nil && recuprarCVVCartao != nil {
        numeroCartaoTextField.text = "\(String(describing: recuprarNumeroCartao!))"
        nomeCartaoTextField.text = "\(String(describing: recuprarNomeCartao!))"
        validadeCartaoTextField.text = "\(String(describing: recuprarValidadeCartao!))"
        cVVCartaoTextField.text = "\(String(describing: recuprarCVVCartao!))"
        }}
    
    func salvarDadosCartao() {
        UserDefaults.standard.set("\(String(describing: self.numeroCartaoTextField.text!))", forKey: "numeroDoCartao")
        UserDefaults.standard.set("\(String(describing: self.nomeCartaoTextField.text!))", forKey: "nomeDoCartao")
        UserDefaults.standard.set("\(String(describing: self.validadeCartaoTextField.text!))", forKey: "validadeDoCartao")
        UserDefaults.standard.set("\(String(describing: self.cVVCartaoTextField.text!))", forKey: "cvvDoCartao")
        }}

extension CheckoutViewController {
    func recuperarCartao() {
        if let recuperarCVVCartao = UserDefaults.standard.object(forKey: "cvvDoCartao") as? String {
            cvvDoCartao = recuperarCVVCartao }
        if let recuperarValidadeCartao = UserDefaults.standard.object(forKey: "validadeDoCartao") as? String {
            validadeDoCartao = recuperarValidadeCartao }
        if let recuperarNumeroCartao = UserDefaults.standard.object(forKey: "numeroDoCartao") as? String {
            let numeroDoCartaoParaOcultar = recuperarNumeroCartao.replacingOccurrences(of: " ", with: "")
            numeroDoCartao = recuperarNumeroCartao.replacingOccurrences(of: " ", with: "")
            var numeroCartaoOcultado: String = ""
            numeroDoCartaoParaOcultar.enumerated().forEach { (index, character) in
                
                    // Espaçamento formatado
                if index % 4 == 0 && index > 0 {
                    numeroCartaoOcultado += " "
                }
                if index < 12 {
                    // Para os digitos antes doe 12
                    numeroCartaoOcultado += "•"
                }else{
                    // Add ultimos 4 digitos
                    numeroCartaoOcultado.append(character)
                }}
            let colorRoxo = UIColor(rgb: 0x7967FF)
            botaoFinalizarPedido.backgroundColor = colorRoxo
            numeroDoCartaoLabel.text = numeroCartaoOcultado
            bandeirinhaCartao.image = #imageLiteral(resourceName: "mastercard")
        }}}
