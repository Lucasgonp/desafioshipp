//
//  APIGooglePlaces.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 25/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

//import APIGooglePlaces

import UIKit
import GooglePlaces
import MapKit

class APIGooglePlaces {
    
    var apiNomeEstab: String?
    var apiEndEstab: String?
    var apiLocationEstab: CLLocationCoordinate2D?
    
}
extension MainViewController: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "ProdutoViewController") as! ProdutoViewController
    
    // Mandar dados para ProdutoViewController
    vc.nomeEstabelecimento = place.name
    vc.endEstabelecimento = place.formattedAddress
    vc.coordinateEstabelecimento = place.coordinate
    vc.placesAPI = place
    estabelecimentoDigitado.text = place.name
    
    //Salvar dados do Estab para API class
    let apiGooglePlaces = APIGooglePlaces()
    apiGooglePlaces.apiNomeEstab = place.name
    apiGooglePlaces.apiEndEstab = place.formattedAddress
    apiGooglePlaces.apiLocationEstab = place.coordinate
    
        self.navigationController?.pushViewController(vc, animated: true)
    
    // Dismiss StoryBoard
    dismiss(animated: true, completion: nil)
  }
    
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss to cancel
    dismiss(animated: true, completion: nil)
  }
}
