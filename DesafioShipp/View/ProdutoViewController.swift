//
//  ProdutoViewController.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 26/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class ProdutoViewController: MainViewController {
    
    @IBOutlet weak var nomeEstabLabel: UILabel?
    @IBOutlet weak var endEstabLabel: UILabel?
    @IBOutlet weak var produtoDigitado: UITextField!
    @IBOutlet weak var botaoFPProduto: UIButton!
    
    var nomeEstabelecimento: String?
    var endEstabelecimento: String?
    var coordinateEstabelecimento: CLLocationCoordinate2D?
    
    let toolbarProduto = UIToolbar()
    
    var placesAPI: GMSPlace?
    
    override func viewDidLoad() {
    super.viewDidLoad()
        if coordinateEstabelecimento != nil {
            
            // Passar String para labels
            nomeEstabLabel?.text = recuperarNomeEstabelecimentoProdutoVC()
            endEstabLabel?.text = recuperarEndEstabelecimentoProdutoVC()
                        
            // Setup Teclado (botão confirmar)
            let toolBarItem = setupKeyboardProduto()
            produtoDigitado.inputAccessoryView = toolBarItem
        }}
    
    // Acao botao finalizar pedido
    @IBAction override func botaoFinalizarPedido(_ sender: Any) {
        if produtoDigitado.text!.count < 3  {
            alertaDadosIncompletos()
        }else{ confirmarProduto() }}
    
    // Acao botao confirmar
    @IBAction func confirmarProduto() {
        if produtoDigitado.text!.count < 3 {
            alertaDadosIncompletos()
        }else{ precoVC() }}
    
    // Acao botao cancelar
    @objc func cancelarProduto() {
        view.endEditing(true)}
    
    // Alerta
    func alertaDadosIncompletos() {
        let alertasClass = AlertaClass()
        let alertaController = alertasClass.alertaInformacoesIncompletas()
        self.present( alertaController , animated: true, completion: nil )}
    
    // recuperar dados
      // recuperar nome
    func recuperarNomeEstabelecimentoProdutoVC() -> String {
      if nomeEstabelecimento != nil {
        let dados = DadosEstabelecimentos(nomeEstab: nomeEstabelecimento!, endEstab: endEstabelecimento!, coordEstab: coordinateEstabelecimento!)
        
    let name = dados.recuperarNomeEstab()
    return name
        }else{
            return "Error: falha ao recuperar nome do estabelecimento"
        }}
      // recuperar end
    func recuperarEndEstabelecimentoProdutoVC() -> String {
      if endEstabelecimento != nil {
        let dados = DadosEstabelecimentos(nomeEstab: nomeEstabelecimento!, endEstab: endEstabelecimento!, coordEstab: coordinateEstabelecimento!)
        
    let end = dados.recuperarEndEstab()
    return end
        }else{
            return "Error: falha ao recuperar nome do estabelecimento"
        }}}
