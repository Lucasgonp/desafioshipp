//
//  MainViewController.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 24/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation

class MainViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var estabelecimentoDigitado: UITextField!
    let gerenciadorLocalizacao = CLLocationManager()
    
    lazy var filter: GMSAutocompleteFilter = {
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        return filter }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gerenciadorLocalizacao.delegate = self
        gerenciadorLocalizacao.requestWhenInUseAuthorization()}
    
    @IBAction func digitandoEstabelecimento(_ sender: Any) {
    estabelecimentoDigitado.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self as GMSAutocompleteViewControllerDelegate
        acController.autocompleteFilter = filter
        present(acController, animated: true, completion: nil)}
    
    // Botao Finalizar Pedido
    @IBAction func botaoFinalizarPedido(_ sender: Any) {
    }
    
    //Tratar localizacao
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            let gerenciarLocalizacao = GerenciarLocalizacao()
            if status != .authorizedWhenInUse && status != .notDetermined {
            let alertaController = gerenciarLocalizacao.tratarAcessoLocal()
            present( alertaController , animated: true, completion: nil )}}}

