//
//  AlertasClass.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 27/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

class AlertaClass {
    
    // Alerta Dados Incompletos
    func alertaInformacoesIncompletas() -> UIAlertController{
    let alertaController = UIAlertController(title: "Informações incompletas",
                                             message: "Por favor, verifique os dados incompletos para prosseguir com a compra!",
                                             preferredStyle: .alert )
    
    let confirmar = UIAlertAction(title: "Confirmar", style: .default , handler: nil )
    alertaController.addAction(confirmar)
    return alertaController
    }}
    
extension CheckoutViewController {
    
    // Alerta Editar
    func alertaEditarCartao() -> UIAlertController{
    let alertaController = UIAlertController(title: "Cartão Cadastrado",
                                             message: "Você deseja editar ou realizar alguma alteração no cartão cadastrado?",
                                             preferredStyle: .alert )
    
        let editar = UIAlertAction(title: "Editar", style: .default) { (editar) in
            self.cadastrarCartaoVC()
        }
    let cancelar = UIAlertAction(title: "Cancelar", style: .default , handler: nil )
    alertaController.addAction(editar)
    alertaController.addAction(cancelar)
    return alertaController
    }}

extension CompraFinalizadaViewController {
    
    // Acompanhar produto 
    func alertaInformacoesFinais() {
    let alertaController = UIAlertController(title: "Seu pedido está a caminho!",
                                             message: "\(String(describing: nomeProduto!)) está a \(String(describing: distanciaFormatada!)) de distância",
                                             preferredStyle: .alert )
    
    let confirmar = UIAlertAction(title: "Beleza!", style: .default , handler: nil )
    alertaController.addAction(confirmar)
    present(alertaController, animated: true, completion: nil)
    }
}

